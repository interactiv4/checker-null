<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 *
 * @SuppressWarnings(PHPMD)
 */

namespace Interactiv4\CheckerNull\Test;

use Interactiv4\CheckerNull\CheckerNull;
use Interactiv4\Contracts\Checker\Api\CheckerInterface;
use Interactiv4\Contracts\DataObject\Api\DataObjectInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

/**
 * Class CheckerNullTest.
 *
 * @internal
 */
class CheckerNullTest extends TestCase
{
    /**
     * Test class exists and is an instance of CheckerInterface.
     */
    public function testInstanceOf(): void
    {
        $checker = new CheckerNull();
        static::assertInstanceOf(CheckerInterface::class, $checker);
    }

    /**
     * Test return value is true.
     */
    public function testReturnValueIsTrue(): void
    {
        // Check without arguments
        $checker = new CheckerNull();
        static::assertTrue($checker->check());

        // Check with arguments
        /** @var DataObjectInterface|MockObject $checkerDataMock */
        $checkerDataMock = $this->getMockForAbstractClass(DataObjectInterface::class);
        $checkerDataMock->expects(static::never())->method(static::anything());

        static::assertTrue($checker->check($checkerDataMock));
    }
}
