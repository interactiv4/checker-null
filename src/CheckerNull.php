<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\CheckerNull;

use Interactiv4\Contracts\Checker\Api\CheckerInterface;
use Interactiv4\Contracts\DataObject\Api\DataObjectInterface;

/**
 * Class CheckerNull.
 *
 * This checker can be used to avoid conditional calls.
 *
 * If no checker is provided to your library, creating a CheckerNull instance
 * is a good way to avoid littering your code with `if ($this->checker) { }`
 * blocks.
 *
 * @api
 */
class CheckerNull implements CheckerInterface
{
    /**
     * Null checker.
     *
     * {@inheritdoc}
     */
    public function check(?DataObjectInterface $data = null): bool
    {
        // noop
        return true;
    }
}
