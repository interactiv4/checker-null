<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\CheckerNull;

use Interactiv4\Contracts\Factory\Api\FactoryInterface;

/**
 * Class CheckerNullFactory.
 *
 * @api
 */
class CheckerNullFactory implements FactoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function create(array $data = []): CheckerNull
    {
        return new CheckerNull();
    }
}
