<?php
declare(strict_types=1);
/**
 * @author Interactiv4 Team
 * @copyright Copyright © Interactiv4 (https://www.interactiv4.com)
 */

namespace Interactiv4\CheckerNull\Api;

use Interactiv4\CheckerNull\CheckerNullFactory;
use Interactiv4\Contracts\Checker\Api\CheckerInterface;
use Interactiv4\Contracts\Checker\Api\CheckerInterfaceFactoryInterface;
use Interactiv4\Contracts\Factory\Api\ObjectFactoryInterface;
use Interactiv4\Contracts\Factory\Factory\Proxy;

/**
 * Class CheckerNullInterfaceFactory.
 *
 * @api
 */
class CheckerNullInterfaceFactory extends Proxy implements CheckerInterfaceFactoryInterface
{
    /**
     * {@inheritdoc}
     */
    public function __construct(
        ObjectFactoryInterface $objectFactory,
        string $factoryClass = CheckerNullFactory::class,
        array $factoryArguments = []
    ) {
        parent::__construct(
            $objectFactory,
            $factoryClass,
            $factoryArguments
        );
    }

    /**
     * Proxy object creation to concrete factory.
     *
     * {@inheritdoc}
     */
    public function create(array $arguments = []): CheckerInterface
    {
        return parent::create($arguments);
    }
}
