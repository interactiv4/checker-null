# Interactiv4 Checker Null

Description
-----------
This package provides null implementation for interactiv4/checker-contracts package.


Versioning
----------
This package follows semver versioning.


Compatibility
-------------
- PHP ^7.1


Installation Instructions
-------------------------
It can be installed manually using composer, by typing the following command:

`composer require interactiv4/checker-null --update-with-all-dependencies`


Support
-------
Refer to [issue tracker](https://bitbucket.org/interactiv4/checker-null/issues) to open an issue if needed.


Credits
---------
Supported and maintained by Interactiv4 Team.


Contribution
------------
Any contribution is highly appreciated.
The best way to contribute code is to open a [pull request on Bitbucket](https://bitbucket.org/interactiv4/checker-null/pull-requests/new).


License
-------
[MIT](https://es.wikipedia.org/wiki/Licencia_MIT)


Copyright
---------
Copyright (c) 2020 Interactiv4 S.L.