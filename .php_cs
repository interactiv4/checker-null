#!/usr/bin/env php
<?php

use PhpCsFixer\Fixer\FunctionNotation\NativeFunctionInvocationFixer;

$finder = PhpCsFixer\Finder::create();

$dirs = [
    'src',
    'tests',
];

foreach ($dirs as $dir) {
    if (file_exists($dir) && is_dir($dir)) {
        $finder->in($dir);
    }
}

return PhpCsFixer\Config::create()
    ->setFinder($finder)
    ->setRules([
        '@PSR2' => true,
        '@PHP71Migration' => true,
        '@PHP71Migration:risky' => true,
        '@PhpCsFixer' => true,
        '@PhpCsFixer:risky' => true,
        '@Symfony' => true,
        '@Symfony:risky' => true,
        '@PHPUnit60Migration:risky' => true,
        // Redefine native function invocations to be prefixed
        'native_function_invocation' => [
            'include' => [NativeFunctionInvocationFixer::SET_INTERNAL],
            'scope' => 'all',
            'strict' => true,
        ],
        // Avoid remove params from phpdoc
        'no_superfluous_phpdoc_tags' => false,
        // No blank line after opening tag
        'blank_line_after_opening_tag' => false,
        'concat_space' => ['spacing' => 'one'],
        'php_unit_test_class_requires_covers' => false,
        'final_internal_class' => false,
    ]);
